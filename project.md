# XServe Project

## Idea

* Old, decommissioned server
* Cluster of RPIs
  * ~ 8 Nodes
  * Gigabit Ethernet
  * USB Storage (Gluster/Ceph or similar)
* No irreversible modifications
  * Maintain overall aesthetics
  * Use existing mounting points to construct 'motherboard'
* Power
  * Internal 5v supply - don't reuse existing
* Network
  * Internal Gig switch
  * External connection exposed via coupling socket

## Disassembly

* GPU on Horizontal Riser (PCI-X?)
* Power supply is mounted internally
  * Fans stuck to front and back
* Two CPUs
  * Daughterboard style
  * Processor Covers mounted using tapered pins
  * Power PC
* Cooling Fans
  * 7 * 0.6A Deltas CPU - wingless plane
  * Grouped into 4 fans + 3 fans
  * Will need to replace these - 5v, low power, quieter (!)
* Front Panel
  * Some sort of parallel connector for power/status LEDs
  * Firewire patch to front panel
  * 3x Sata patches to backplane
* Slot Load CD Drive
* Case Lock
  * Magnetic (?) sensor to detect position
* Motherboard
  * Overall mounting is to place the board down over 2 pins, then slide it backwards towards rear IO
  * Reinforcement under CPU mounts in form of metal framework
* Vanity Covers (?)
  * One each side covering IO ports/fan headers
  * Clip into side of Case
  * RHS containing Fan
    * Mounts securely with board removed, additional mounting to chassis
  * LHS
    * Mounts fairly insecurely, will require additional support
* Fixed 3x drive backplane mounted to case

### Pictures

* ![Main Board](images/XSERVE-Overall.jpg)
* ![CPU Area](images/XSERVE-CPU.jpg)
* ![FrontIO - Power Switch](images/XSERVE-FRONTIO-PowerSwitch.jpg)
* ![FrontIO - Patches](images/XSERVE-FRONTIO-Patch.jpg)
* ![FrontIO - Lock Detector](images/XSERVE-FRONTIO-LockDetector.jpg)
* ![CD Drive](images/XSERVE-CdDrive.jpg)
* ![CPU Cover](images/XSERVE-CPUCover.jpg)
* ![CPU Cover - Underside](images/XSERVE-CPUCoverUnderside.jpg)
* ![CPU Fan](images/XSERVE-CPUFan.jpg)
* ![Mobo Underside](images/XSERVE-MOBO-Underside.jpg)
* ![Mobo Undertray](images/XSERVE-MOBO-Undertray.jpg)
* ![CPU Socket](images/XSERVE-CPUSocket.jpg)
  

## Planning and Design

* Initial Design
  * Model the Server
    * Simple model just to test position of mounting points
    * None of the 4 points are square with the frame
    * ![Server Model](images/XSERVE-FUSION-Chassis.png)
  * Template 'Spider' to check model
    * Use points in model to create a template motherboard
    * Add right angle area to check for rotation of mountings
    * ![Server Spider](images/XSERVE-FUSION-TemplateSpider.png)
    * ![Server Spider Printed](images/XSERVE-MOUNT-Initial.jpg)
  * Draw an initial layout to check possibility
    * Check physical space is sufficient based on known components
    * ![Drawn Layout](images/XSERVE-MOUNT-Layout.jpg)
* Parts
  * TP Link Switches
    * Model switch mounting points, create inverse template
    * ![Switch](images/TPLINK-SwitchPCB.jpg)
    * ![Switch Model](images/TPLINK-FUSION-Model.png)
    * ![Switch Spider](images/TPLINK-FUSION-Spider.png)
    * ![Switch Model+Spider](images/TPLINK-FUSION-SpiderModel.png)
    * ![Initial Fitment](images/TPLINK-SpiderTest.jpg)
    * ![Initial Fitment](images/TPLINK-SpiderTestUnderside.jpg)
  * Pi4 4GB
    * Model constructed from mechanical drawings published by RPI Foundation
    * ![Pi Model](images/RPI-FUSION-Model.png)
    * ![Pi Spider](images/RPI-FUSION-Spider.png)
    * ![Initial Fitment](images/RPI-SpiderTest.jpg)
    * ![Initial Fitment](images/RPI-SpiderTestUnderside.jpg)
  * CPU Covers
    * Tapered pin, needs to hold cover snugly, but remain removable
    * ![Model](images/XSERVE-FUSION-CPUCoverMountTest.png)
    * ![Model Halved](images/XSERVE-FUSION-CPUCoverMountTestHalf.png)
    * ![Initial Fitment](images/XSERVE-CPUCoverTest.jpg)
    * ![Initial Fitment](images/XSERVE-CPUCoverTest2.jpg)
  * Meanwell 5V PSU
    * Delivery Delays
  * Noctua 5V Fan
    * Designed to replace original delta fans
    * Wire all to single cable, single PWM output, 1x providedRPM sense
